import React from 'react';

const SomeBreadcrumbs = () => {
    return (
        <div className="breadcrumbs-wrapper">
            <span className="breadcrumbs">
                Overview ‣ List of pools ‣{' '}
                <span className="breadcrumbs breadcrumbs-active">Vandelay Trust</span>
            </span>
        </div>
    );
};

export default SomeBreadcrumbs;
