import React from 'react';
import '@patternfly/react-core/dist/styles/base.css';
import { ChartDonutUtilization, ChartLabel, ChartThemeColor } from '@patternfly/react-charts';

const DonutChart = ({ title, x, y, spanClass }) => (
    <div className="donut-chart-wrapper">
        <ChartDonutUtilization
            constrainToVisibleArea={true}
            data={{ x: x, y: y}}
            labels={({ datum }) => (datum.x ? `${datum.x}: ${datum.y}` : null)}
            title={title}
            innerRadius={70}
            width={150}
            height={150}
            endAngle={360}
            // style={[{ fill: 'red', fontSize: 24 }  ]}
            themeColor={ChartThemeColor.blue}
        />
        <span className={`${spanClass} donut-chart__span`}>{x}</span>
    </div>
);
export default DonutChart;
