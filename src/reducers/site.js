import * as reduxConstants from '../constants/reduxConstants';
import {store} from '../store/index';

const initialState = {
    siteData: null,
    loading: false,
    error: null
};

export const siteDataReducer = (state = initialState, action) => {
    const { type, payload } = action;
    switch (type) {
        case reduxConstants.FETCH_SITE_DATA:
            return { ...state, loading: true };
        case reduxConstants.FETCH_SITE_DATA_SUCCESS:
            return { ...state, loading: false, siteData: payload };
        case reduxConstants.FETCH_SITE_DATA_ERROR:
            return { ...state, loading: false, error: payload };
        default:
            return state;
    }
};