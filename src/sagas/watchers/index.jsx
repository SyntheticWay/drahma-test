import { all } from 'redux-saga/effects';
import siteWatchers from './siteWatcher';

export default function* watcher() {
    yield all([
        siteWatchers(),
    ]);
}
