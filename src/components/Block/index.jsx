import React from 'react';
import BlockItem from './BlockItem';

const Block = ({ data }) => {
    const { siteData } = data;
    return (
        <>
            {siteData &&
                Object.values(siteData).map((element) => {
                    return (
                        <div className="block-wrapper" key={element.id}>
                            <div className="block-title-wrapper">
                                <img
                                    className="block-parth-icon"
                                    src={`/src/assets/icons/${element.image}.svg`}
                                    alt={element.image}
                                />
                                <span className="block-parth-title">
                                    {element.parth_title}
                                </span>
                            </div>
                            <div className="block-content">
                                {element?.items.map((item) => (
                                    <BlockItem item={item} key={item.id} />
                                ))}
                            </div>
                        </div>
                    );
                })}
        </>
    );
};

export default Block;
