import { combineReducers } from 'redux';
import { siteDataReducer } from './site';

export const rootReducer = combineReducers({
    siteData: siteDataReducer
});
