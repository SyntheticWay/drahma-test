import React from 'react';
import { useSelector } from 'react-redux';
import Block from '../Block';

const SomePage = () => {
    const { siteData } = useSelector((state) => state);
    return (
        <div className="page-wrapper">
            <div className="blocks-wrapper">
            <Block data={siteData} />
            </div>
        </div>
    );
};

export default SomePage;
