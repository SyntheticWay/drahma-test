import React from 'react';
import { useSelector } from 'react-redux';

const Header = () => {
    const { siteData } = useSelector((state) => state);
    const data = siteData.siteData;
    return (
        <div className="header-wrapper">
            <img
                className="header-logo"
                src={`/src/assets/icons/logo.svg`}
                alt='logo'
            />
            {data &&
                Object.values(data).map((item) => (
                    <div className="header-item" key={item.id}>
                        {item.parth_title}
                    </div>
                ))}
        </div>
    );
};

export default Header;
