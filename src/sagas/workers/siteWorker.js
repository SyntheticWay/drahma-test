import { call, put } from 'redux-saga/effects';
import * as reduxConstants from '../../constants/reduxConstants';
import {requestSimulation} from '../../backend/index';

export function* fetchSiteData() {
    try {
        const {data} = requestSimulation();
        if (data) {
            yield put({
                type: reduxConstants.FETCH_SITE_DATA_SUCCESS,
                payload: data,
            });
        }
    } catch (error) {
        yield put({type: reduxConstants.FETCH_SITE_DATA_ERROR, payload: error});
    }
}