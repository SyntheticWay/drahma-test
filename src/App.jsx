import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchSiteDataAction } from './actions/siteActions';
import { bindActionCreators } from 'redux';

import './App.css';
import SomePage from './components/SomePage';
import SomeBreadcrumbs from './components/SomeBreadcrumbs';
import Header from './components/Header';

class App extends Component {
    componentDidMount() {
        const { fetchSiteData } = this.props;
        fetchSiteData();
    }

    render() {
        return (
            <div className="App">
                <Header />
                <main className="main">
                    <section className='section'>
                        <SomeBreadcrumbs />
                        <SomePage />
                    </section>
                    <aside className="aside">
                        <div className="sidebar">Sidebar</div>
                    </aside>
                </main>
            </div>
        );
    }
}

const mapDispatchToProps = (dispatch) => ({
    fetchSiteData: bindActionCreators(fetchSiteDataAction, dispatch)
});

export default connect(null, mapDispatchToProps)(App);
