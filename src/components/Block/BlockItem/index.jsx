import React from 'react';
import DonutChart from '../../DonutChart';

const BlockItem = ({ item }) => {
    let value = '';
    if (item.value > 10000) {
        value = `${(item.value + '').substr(0, 2)}K`;
    } else if (!item.isDiagram && item.value > 1000) {
        value = `${item.value} +`;
    } else {
        value = item.value;
    }
    return (
        <div className="block-content-item">
            {item?.isDiagram ? (
                <DonutChart title={value} x={item.title} y={item.value} spanClass={'block-content-item__title'} />
            ) : (
                <>
                    <span className="block-content-item__title">
                        {item.title}
                    </span>
                    <span className="block-content-item__value">{value}</span>
                </>
            )}

            {item.this_month && (
                <span className="item__description">this month</span>
            )}
        </div>
    );
};

export default BlockItem;
