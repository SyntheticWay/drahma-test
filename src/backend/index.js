const data_obj = {
    data: {
        pool_capacity: {
            parth_title: 'Pool capacity',
            image: 'pool_capacity',
            items: [
                { title: 'Allocated', value: 17000, isDiagram: true },
                { title: 'Locked', value: 300, isDiagram: true },
                { title: 'Available', value: 1234, isDiagram: true },
                { title: 'Total', value: 25000, isDiagram: true }
            ]
        },
        processes: {
            parth_title: 'Processes',
            image: 'processes',
            items: [
                { title: 'Awards created', value: 42, this_month: true },
                { title: 'Candeled', value: 12, this_month: true },
                { title: 'Running', value: 1400, this_month: true },
                { title: 'Completed', value: 125, this_month: true }
            ]
        },
        participants: {
            parth_title: 'Participants',
            image: 'participants',
            items: [
                { title: 'active', value: 150 },
                { title: 'Newcommers', value: 15, this_month: true },
                { title: 'Leavers', value: 55 },
                { title: 'Missing ID', value: 2 }
            ]
        }
    }
};

export const requestSimulation = () => {
    return data_obj;
};
