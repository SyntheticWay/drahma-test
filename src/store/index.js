import { applyMiddleware, createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import createSagaMiddleware from 'redux-saga';
import { rootReducer } from '../reducers';
import watcher from '../sagas/watchers';

const sagaMiddleware = createSagaMiddleware();

const composeEnhancers = composeWithDevTools({
    trace: true,
    traceLimit: 25
});

function configureStore() {
    const store = createStore(
        rootReducer,
        composeEnhancers(applyMiddleware(sagaMiddleware))
    );
    sagaMiddleware.run(watcher);
    return store;
}

export const store = configureStore();