export const FETCH_SITE_DATA = 'FETCH_SITE_DATA';
export const FETCH_SITE_DATA_SUCCESS = 'FETCH_SITE_DATA_SUCCESS';
export const FETCH_SITE_DATA_ERROR = 'FETCH_SITE_DATA_ERROR';
