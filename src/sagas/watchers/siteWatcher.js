// import { takeEvery } from 'redux-saga/effects';
import * as sagaConstants from '../../constants/sagaConstants';
import {fetchSiteData} from '../workers/siteWorker';
import { takeLatest } from 'redux-saga/effects';

export default function* siteWatchers() {
  yield takeLatest(sagaConstants.FETCH_SITE_DATA_REQ, fetchSiteData);
}