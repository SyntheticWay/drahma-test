import * as sagaConstants from '../constants/sagaConstants';

export const fetchSiteDataAction = () => ({
  type: sagaConstants.FETCH_SITE_DATA_REQ,
});

// export const fetchSiteSettingsAction = () => ({
//   type: sagaConstants.FETCH_SITE_SETTINGS_REQ,
// });